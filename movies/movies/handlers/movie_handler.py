import json
import requests
from flask import request, jsonify
from flask_restful import Resource
from movies.exceptions import BadUserInput, MovieNotFound, ApiKeyNotFound, InternalServerError
from movies.managers.movie_manager import MovieManager


class MovieHandler(Resource):

    def __init__(self, movie_manager: MovieManager):
        self.movie_manager = movie_manager

    def get(self):
        try:
            search = request.args.get('search')
            if search:
                page_size = int(request.args.get('page_size', 10))
                page = int(request.args.get('page', 1))
                if not page_size or not page:
                    raise BadUserInput
                movies = self.movie_manager.get_movies(search=search, page_size=page_size, page=page)
                response = jsonify({
                    "status": "success",
                    "data": movies
                })
                return response

            else:
                title = request.args.get('title')
                movie = self.movie_manager.get_movie(title=title)
                response = jsonify({
                    "status": "success",
                    "data": movie
                })
                return response

        except (BadUserInput, MovieNotFound) as exc:
            response = jsonify({
                "status": "error",
                "message": exc.reason
            })
            response.status_code = exc.code
            return response

        except Exception:
            response = jsonify({
                "status": "error",
                "message": InternalServerError.reason
            })
            response.status_code = InternalServerError.code
            return response

    def post(self):
        try:
            title = request.args.get('title')
            api_url = f'https://www.omdbapi.com/?apikey=ed8853f9&type=movie&t={title}'
            response = requests.get(api_url)
            data = json.loads(response.text)
            if 'Error' in data:
                raise MovieNotFound
            if data['Title'].lower() != title.lower():
                raise MovieNotFound
            title = data['Title']
            year = data['Year']
            imdb_id = data['imdbID']
            type = data['Type']
            poster = data['Poster']
            self.movie_manager.add_movie(title=title, year=year, imdb_id=imdb_id, type=type, poster=poster)
            return jsonify({"status": "success", "message": "Movie successfully added to the database"})

        except (BadUserInput, MovieNotFound) as exc:
            response = jsonify({
                "status": "error",
                "message": exc.reason
            })
            response.status_code = exc.code
            return response

        except Exception:
            response = jsonify({
                "status": "error",
                "message": InternalServerError.reason
            })
            response.status_code = InternalServerError.code
            return response

    def delete(self):
        try:
            imdb_id = request.args.get('id')
            apikey = request.args.get('apikey')
            deleted_count = self.movie_manager.delete_movie(imdb_id=imdb_id, apikey=apikey)
            if not deleted_count:
                raise MovieNotFound
            return jsonify({"status": "success", "message": "Movie successfully deleted from the database"})

        except (BadUserInput, ApiKeyNotFound, MovieNotFound) as exc:
            response = jsonify({
                "status": "error",
                "message": exc.reason
            })
            response.status_code = exc.code
            return response

        except Exception:
            response = jsonify({
                "status": "error",
                "message": InternalServerError.reason
            })
            response.status_code = InternalServerError.code
            return response
