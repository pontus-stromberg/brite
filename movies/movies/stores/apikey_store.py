from pymongo.collection import Collection
from movies.exceptions import BadUserInput


class ApiKeyStore:

    def __init__(self, collection: Collection):
        self.collection = collection

    def validate_key(self, apikey: str):
        if not apikey:
            raise BadUserInput
        filter = {'apikey': apikey}
        return self.collection.find_one(filter=filter)
