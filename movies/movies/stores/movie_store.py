from pymongo.collection import Collection
from movies.exceptions import BadUserInput, MovieNotFound


class MovieStore:

    def __init__(self, collection: Collection):
        self.collection = collection

    def get_movie(self, title: str):
        if not title:
            raise BadUserInput
        filter = {'Title': title}
        movie = self.collection.find_one(filter=filter, projection={'_id': 0})
        if not movie:
            raise MovieNotFound
        return movie

    def get_movies(self, search: str, page_size: int = 10, page: int = 1):
        if not search or page_size < 1 or page < 1:
            raise BadUserInput
        filter = {"Title": {"$regex": search, "$options": "i"}}
        skip_count = (page - 1) * page_size
        result = self.collection.find(
            filter=filter, projection={'_id': 0}).sort('Title').skip(skip_count).limit(page_size)
        movies = list(result)
        if not movies:
            raise MovieNotFound
        return movies

    def add_movie(self, title: str, year: str, imdb_id: str, type: str, poster: str):
        if title and year and imdb_id and type and poster:
            return self.collection.insert_one({
                'Title': title,
                'Year': year,
                'imdbID': imdb_id,
                'Type': type,
                'Poster': poster
            })
        else:
            raise BadUserInput

    def delete_movie(self, imdb_id: str):
        if not imdb_id:
            raise BadUserInput
        return self.collection.delete_one(filter={'imdbID': imdb_id}).deleted_count
