import uuid
import pytest
from flask import Flask
from flask_restful import Api
from mongomock import MongoClient
from movies.handlers.movie_handler import MovieHandler
from movies.managers.movie_manager import MovieManager
from movies.stores.apikey_store import ApiKeyStore
from movies.stores.movie_store import MovieStore


@pytest.fixture(scope="session")
def apikey():
    return uuid.uuid4().hex


@pytest.fixture
def mongo_collection():
    mongo_client = MongoClient()
    db = mongo_client['test_database']
    collection = db['test_collection']
    return collection


@pytest.fixture
def movie_store(mongo_collection):
    return MovieStore(mongo_collection)


@pytest.fixture
def apikey_store(mongo_collection, apikey):
    store = ApiKeyStore(mongo_collection)
    store.collection.insert_one({'apikey': apikey})
    return store


@pytest.fixture
def movie_manager(movie_store, apikey_store):
    return MovieManager(movie_store=movie_store, apikey_store=apikey_store)


@pytest.fixture
def movie_client(movie_manager):
    app = Flask(__name__)
    api = Api(app)
    api.add_resource(MovieHandler, '/movies', resource_class_kwargs={'movie_manager': movie_manager})
    return app.test_client()


def get_sample_movie():
    return {
        'Title': uuid.uuid4().hex,
        'Year': uuid.uuid4().hex,
        'imdbID': uuid.uuid4().hex,
        'Type': uuid.uuid4().hex,
        'Poster': uuid.uuid4().hex
    }
