import uuid
import pytest
from movies.exceptions import BadUserInput


class TestApiKeyStore:

    def test_validate_key(self, apikey_store, apikey):
        result = apikey_store.validate_key(apikey=apikey)
        assert result

    def test_validate_wrong_key(self, apikey_store):
        result = apikey_store.validate_key(apikey=uuid.uuid4().hex)
        assert not result

    def test_validate_key_bad_input(self, apikey_store):
        with pytest.raises(BadUserInput):
            apikey_store.validate_key(apikey=None)
