import json
import random
import uuid
from unittest.mock import MagicMock
import requests
from flask import Response
from movies.exceptions import BadUserInput, MovieNotFound, ApiKeyNotFound, InternalServerError
from movies.tests.conftest import get_sample_movie


class TestMovieHandler:

    def test_get_movie(self, movie_client, movie_store):
        sample_movie = get_sample_movie()
        movie_store.collection.insert_one(sample_movie)
        input_data = {
            "title": sample_movie['Title']
        }
        response = movie_client.get('/movies', query_string=input_data)
        response_data = json.loads(response.text)
        assert response_data['status'] == 'success'
        assert response_data['data']['Title'] == sample_movie['Title']
        assert response_data['data']['Year'] == sample_movie['Year']
        assert response_data['data']['imdbID'] == sample_movie['imdbID']
        assert response_data['data']['Type'] == sample_movie['Type']
        assert response_data['data']['Poster'] == sample_movie['Poster']

    def test_get_movies(self, movie_client, movie_store):
        sample_movie1 = get_sample_movie()
        sample_movie1['Title'] = 'Title1'
        movie_store.collection.insert_one(sample_movie1)
        sample_movie2 = get_sample_movie()
        sample_movie2['Title'] = 'Title2'
        movie_store.collection.insert_one(sample_movie2)
        sample_movie3 = get_sample_movie()
        sample_movie3['Title'] = uuid.uuid4().hex
        movie_store.collection.insert_one(sample_movie3)
        input_data = {
            "search": 'Title'
        }
        response = movie_client.get('/movies', query_string=input_data)
        response_data = json.loads(response.text)
        assert response.status_code == 200
        assert response_data['status'] == 'success'
        assert len(response_data['data']) == 2
        titles = [response_data['data'][0]['Title'], response_data['data'][1]['Title']]
        assert sample_movie1['Title'] in titles
        assert sample_movie2['Title'] in titles

    def test_get_movies_page_size(self, movie_client, movie_store):
        for i in range(10):
            sample_movie = get_sample_movie()
            sample_movie['Title'] = f'Title{i}'
            movie_store.collection.insert_one(sample_movie)
        page_size = random.randint(1, 5)
        input_data = {
            "search": 'Title',
            "page_size": page_size
        }
        response = movie_client.get('/movies', query_string=input_data)
        response_data = json.loads(response.text)
        assert len(response_data['data']) == page_size

    def test_get_movies_page_nbr(self, movie_client, movie_store):
        for i in range(26):
            sample_movie = get_sample_movie()
            sample_movie['Title'] = f'Title{i}'
            movie_store.collection.insert_one(sample_movie)

        input_data = {
            "search": 'Title',
            "page": 1
        }
        response = movie_client.get('/movies', query_string=input_data)
        response_data = json.loads(response.text)
        assert len(response_data['data']) == 10

        input_data = {
            "search": 'Title',
            "page": 2
        }
        response = movie_client.get('/movies', query_string=input_data)
        response_data = json.loads(response.text)
        assert len(response_data['data']) == 10

        input_data = {
            "search": 'Title',
            "page": 3
        }
        response = movie_client.get('/movies', query_string=input_data)
        response_data = json.loads(response.text)
        assert len(response_data['data']) == 6

    def test_get_bad_user_input(self, movie_client, movie_manager):
        movie_manager.get_movie = MagicMock(side_effect=BadUserInput)
        input_data = {
            "title": uuid.uuid4().hex
        }
        response = movie_client.get('/movies', query_string=input_data)
        response_data = json.loads(response.text)
        assert response.status_code == BadUserInput.code
        assert response_data['status'] == 'error'
        assert response_data['message'] == BadUserInput.reason

    def test_get_bad_not_found(self, movie_client, movie_manager):
        movie_manager.get_movie = MagicMock(side_effect=MovieNotFound)
        input_data = {
            "title": uuid.uuid4().hex
        }
        response = movie_client.get('/movies', query_string=input_data)
        response_data = json.loads(response.text)
        assert response.status_code == MovieNotFound.code
        assert response_data['status'] == 'error'
        assert response_data['message'] == MovieNotFound.reason

    def test_get_bad_internal_server_error(self, movie_client, movie_manager):
        movie_manager.get_movie = MagicMock(side_effect=Exception)
        input_data = {
            "title": uuid.uuid4().hex
        }
        response = movie_client.get('/movies', query_string=input_data)
        response_data = json.loads(response.text)
        assert response.status_code == InternalServerError.code
        assert response_data['status'] == 'error'
        assert response_data['message'] == InternalServerError.reason

    def test_add_movie(self, movie_client, movie_manager):
        sample_movie = get_sample_movie()
        input_data = {
            "title": sample_movie['Title']
        }
        requests.get = MagicMock()
        mocked_response = Response()
        mocked_response.text = json.dumps({
            'Title': sample_movie['Title'],
            'Year': sample_movie['Year'],
            'imdbID': sample_movie['imdbID'],
            'Type': sample_movie['Type'],
            'Poster': sample_movie['Poster']

        })
        requests.get.return_value = mocked_response
        response = movie_client.post('/movies', query_string=input_data)
        response_data = json.loads(response.text)
        assert response.status_code == 200
        assert response_data['status'] == 'success'
        assert response_data['message'] == 'Movie successfully added to the database'

    def test_add_movie_api_error(self, movie_client, movie_manager):
        sample_movie = get_sample_movie()
        input_data = {
            "title": sample_movie['Title']
        }
        requests.get = MagicMock()
        mocked_response = Response()
        mocked_response.text = json.dumps({
            'Error': uuid.uuid4().hex
        })
        requests.get.return_value = mocked_response
        response = movie_client.post('/movies', query_string=input_data)
        response_data = json.loads(response.text)
        assert response.status_code == MovieNotFound.code
        assert response_data['status'] == 'error'
        assert response_data['message'] == MovieNotFound.reason

    def test_add_movie_wrong_title(self, movie_client, movie_manager):
        sample_movie = get_sample_movie()
        input_data = {
            "title": sample_movie['Title']
        }
        requests.get = MagicMock()
        mocked_response = Response()
        mocked_response.text = json.dumps({
            'Title': uuid.uuid4().hex,
            'Year': sample_movie['Year'],
            'imdbID': sample_movie['imdbID'],
            'Type': sample_movie['Type'],
            'Poster': sample_movie['Poster']

        })
        requests.get.return_value = mocked_response
        response = movie_client.post('/movies', query_string=input_data)
        response_data = json.loads(response.text)
        assert response.status_code == MovieNotFound.code
        assert response_data['status'] == 'error'
        assert response_data['message'] == MovieNotFound.reason

    def test_add_movie_bad_input(self, movie_client, movie_manager):
        sample_movie = get_sample_movie()
        input_data = {
            "title": sample_movie['Title']
        }
        requests.get = MagicMock()
        mocked_response = Response()
        mocked_response.text = json.dumps({
            'Title': sample_movie['Title'],
            'Year': sample_movie['Year'],
            'imdbID': sample_movie['imdbID'],
            'Type': sample_movie['Type'],
            'Poster': sample_movie['Poster']

        })
        requests.get.return_value = mocked_response
        movie_manager.add_movie = MagicMock(side_effect=BadUserInput)
        response = movie_client.post('/movies', query_string=input_data)
        response_data = json.loads(response.text)
        assert response.status_code == BadUserInput.code
        assert response_data['status'] == 'error'
        assert response_data['message'] == BadUserInput.reason

    def test_add_movie_internal_server_error(self, movie_client, movie_manager):
        sample_movie = get_sample_movie()
        input_data = {
            "title": sample_movie['Title']
        }
        requests.get = MagicMock()
        mocked_response = Response()
        mocked_response.text = json.dumps({
            'Title': sample_movie['Title'],
            'Year': sample_movie['Year'],
            'imdbID': sample_movie['imdbID'],
            'Type': sample_movie['Type'],
            'Poster': sample_movie['Poster']

        })
        requests.get.return_value = mocked_response
        movie_manager.add_movie = MagicMock(side_effect=Exception)
        response = movie_client.post('/movies', query_string=input_data)
        response_data = json.loads(response.text)
        assert response.status_code == InternalServerError.code
        assert response_data['status'] == 'error'
        assert response_data['message'] == InternalServerError.reason

    def test_delete_movie(self, movie_client, movie_store, apikey):
        sample_movie = get_sample_movie()
        movie_store.collection.insert_one(sample_movie)
        input_data = {
            "id": sample_movie['imdbID'],
            "apikey": apikey
        }
        response = movie_client.delete('/movies', query_string=input_data)
        response_data = json.loads(response.text)
        assert response.status_code == 200
        assert response_data['status'] == 'success'
        assert response_data['message'] == "Movie successfully deleted from the database"

    def test_delete_movie_not_found(self, movie_client, apikey):
        input_data = {
            "id": uuid.uuid4().hex,
            "apikey": apikey
        }
        response = movie_client.delete('/movies', query_string=input_data)
        response_data = json.loads(response.text)
        assert response.status_code == MovieNotFound.code
        assert response_data['status'] == 'error'
        assert response_data['message'] == MovieNotFound.reason

    def test_delete_movie_bad_input(self, movie_client, movie_manager, apikey):
        input_data = {
            "id": uuid.uuid4().hex,
            "apikey": apikey
        }
        movie_manager.delete_movie = MagicMock(side_effect=BadUserInput)
        response = movie_client.delete('/movies', query_string=input_data)
        response_data = json.loads(response.text)
        assert response.status_code == BadUserInput.code
        assert response_data['status'] == 'error'
        assert response_data['message'] == BadUserInput.reason

    def test_delete_movie_apikey_not_found(self, movie_client, movie_manager, movie_store):
        sample_movie = get_sample_movie()
        movie_store.collection.insert_one(sample_movie)
        input_data = {
            "id": sample_movie['imdbID'],
            "apikey": uuid.uuid4().hex
        }
        response = movie_client.delete('/movies', query_string=input_data)
        response_data = json.loads(response.text)
        assert response.status_code == ApiKeyNotFound.code
        assert response_data['status'] == 'error'
        assert response_data['message'] == ApiKeyNotFound.reason

    def test_delete_movie_internal_server_error(self, movie_client, movie_manager, apikey):
        input_data = {
            "id": uuid.uuid4().hex,
            "apikey": apikey
        }
        movie_manager.delete_movie = MagicMock(side_effect=Exception)
        response = movie_client.delete('/movies', query_string=input_data)
        response_data = json.loads(response.text)
        assert response.status_code == InternalServerError.code
        assert response_data['status'] == 'error'
        assert response_data['message'] == InternalServerError.reason