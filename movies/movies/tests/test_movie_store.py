import random
import uuid
import pytest
from movies.exceptions import BadUserInput, MovieNotFound
from movies.tests.conftest import get_sample_movie


class TestMovieStore:

    def test_get_movie(self, movie_store):
        sample_movie = get_sample_movie()
        movie_store.collection.insert_one(sample_movie)
        sample_movie2 = get_sample_movie()
        movie_store.collection.insert_one(sample_movie2)
        movie = movie_store.get_movie(title=sample_movie['Title'])
        assert movie['Title'] == sample_movie['Title']
        assert movie['Year'] == sample_movie['Year']
        assert movie['imdbID'] == sample_movie['imdbID']
        assert movie['Type'] == sample_movie['Type']
        assert movie['Poster'] == sample_movie['Poster']

    def test_get_movie_not_found(self, movie_store):
        with pytest.raises(MovieNotFound):
            movie_store.get_movie(title=uuid.uuid4().hex)

    def test_get_movie_bad_input(self, movie_store):
        with pytest.raises(BadUserInput):
            movie_store.get_movie(title=None)

    def test_get_movies_filter_title(self, movie_store):
        sample_movie1 = get_sample_movie()
        sample_movie1['Title'] = 'Title2'
        movie_store.collection.insert_one(sample_movie1)
        sample_movie2 = get_sample_movie()
        sample_movie2['Title'] = 'Title1'
        movie_store.collection.insert_one(sample_movie2)
        sample_movie3 = get_sample_movie()
        sample_movie3['Title'] = 'XXXX'
        movie_store.collection.insert_one(sample_movie3)
        movies = movie_store.get_movies(search='Title')
        assert movies[0]['Title'] == sample_movie2['Title']
        assert movies[1]['Title'] == sample_movie1['Title']
        assert len(movies) == 2

    def test_get_movies_page_size(self, movie_store):
        for i in range(10):
            sample_movie = get_sample_movie()
            sample_movie['Title'] = f'Title{i}'
            movie_store.collection.insert_one(sample_movie)
        page_size = random.randint(1, 5)
        movies = movie_store.get_movies(search='Title', page_size=page_size)
        assert len(movies) == page_size

    def test_get_movies_page_nbr(self, movie_store):
        for i in range(26):
            sample_movie = get_sample_movie()
            sample_movie['Title'] = f'Title{i}'
            movie_store.collection.insert_one(sample_movie)
        movies = movie_store.get_movies(search='Title', page=1)
        assert len(movies) == 10
        movies = movie_store.get_movies(search='Title', page=2)
        assert len(movies) == 10
        movies = movie_store.get_movies(search='Title', page=3)
        assert len(movies) == 6

    def test_get_movies_bad_input(self, movie_store):
        with pytest.raises(BadUserInput):
            movie_store.get_movies(search='')

        with pytest.raises(BadUserInput):
            movie_store.get_movies(search=uuid.uuid4().hex, page_size=0)

        with pytest.raises(BadUserInput):
            movie_store.get_movies(search=uuid.uuid4().hex, page=0)

    def test_add_movie(self, movie_store):
        sample_movie = get_sample_movie()
        movie_store.add_movie(title=sample_movie['Title'], year=sample_movie['Year'],
                              imdb_id=sample_movie['imdbID'], type=sample_movie['Type'],
                              poster=sample_movie['Poster'])
        movie = movie_store.collection.find_one(filter={'Title': sample_movie['Title']})
        assert movie['Title'] == sample_movie['Title']
        assert movie['Year'] == sample_movie['Year']
        assert movie['imdbID'] == sample_movie['imdbID']
        assert movie['Type'] == sample_movie['Type']
        assert movie['Poster'] == sample_movie['Poster']

    def test_add_movie_bad_input(self, movie_store):
        sample_movie = get_sample_movie()
        with pytest.raises(BadUserInput):
            movie_store.add_movie(title=None, year=sample_movie['Year'],
                                  imdb_id=sample_movie['imdbID'], type=sample_movie['Type'],
                                  poster=sample_movie['Poster'])

        with pytest.raises(BadUserInput):
            movie_store.add_movie(title=sample_movie['Title'], year=None,
                                  imdb_id=sample_movie['imdbID'], type=sample_movie['Type'],
                                  poster=sample_movie['Poster'])

        with pytest.raises(BadUserInput):
            movie_store.add_movie(title=sample_movie['Title'], year=sample_movie['Year'],
                                  imdb_id=None, type=sample_movie['Type'],
                                  poster=sample_movie['Poster'])

        with pytest.raises(BadUserInput):
            movie_store.add_movie(title=sample_movie['Title'], year=sample_movie['Year'],
                                  imdb_id=sample_movie['imdbID'], type=None,
                                  poster=sample_movie['Poster'])

        with pytest.raises(BadUserInput):
            movie_store.add_movie(title=sample_movie['Title'], year=sample_movie['Year'],
                                  imdb_id=sample_movie['imdbID'], type=sample_movie['Type'],
                                  poster=None)

    def test_delete_movie(self, movie_store):
        sample_movie = get_sample_movie()
        movie_store.collection.insert_one(sample_movie)
        deleted_count = movie_store.delete_movie(imdb_id=sample_movie['imdbID'])
        result = movie_store.collection.find_one(filter={'imdbID': sample_movie['imdbID']})
        assert deleted_count == 1
        assert not result

    def test_delete_movie_bad_input(self, movie_store):
        with pytest.raises(BadUserInput):
            movie_store.delete_movie(imdb_id=None)
