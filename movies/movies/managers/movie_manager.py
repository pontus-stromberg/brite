from movies.exceptions import BadUserInput, ApiKeyNotFound
from movies.stores.apikey_store import ApiKeyStore
from movies.stores.movie_store import MovieStore


class MovieManager:

    def __init__(self, movie_store: MovieStore, apikey_store: ApiKeyStore):
        self.movie_store = movie_store
        self.apikey_store = apikey_store

    def get_movie(self, title: str):
        if not title:
            raise BadUserInput
        return self.movie_store.get_movie(title=title)

    def get_movies(self, search: str = '', page_size: int = 10, page: int = 1):
        return self.movie_store.get_movies(search=search, page_size=page_size, page=page)

    def add_movie(self, title: str, year: str, imdb_id: str, type: str, poster: str):
        return self.movie_store.add_movie(title=title, year=year, imdb_id=imdb_id, type=type, poster=poster)

    def delete_movie(self, imdb_id: str, apikey: str):
        if not self.apikey_store.validate_key(apikey=apikey):
            raise ApiKeyNotFound
        return self.movie_store.delete_movie(imdb_id=imdb_id)
