class BadUserInput(Exception):
    reason = 'Bad user input'
    code = 400


class ApiKeyNotFound(Exception):
    reason = 'Api key not valid'
    code = 403


class MovieNotFound(Exception):
    reason = 'Movie not found'
    code = 404


class InternalServerError(Exception):
    reason = 'Internal server error'
    code = 500
