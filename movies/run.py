import os
import requests
from flask import Flask
from flask_restful import Api
from pymongo import MongoClient
from movies.handlers.movie_handler import MovieHandler
from movies.managers.movie_manager import MovieManager
from movies.stores.apikey_store import ApiKeyStore
from movies.stores.movie_store import MovieStore

app = Flask(__name__)
api = Api(app)

mongo_client: MongoClient = MongoClient(host='mongodb', port=27017)


db = mongo_client["movies"]
apikey_collection = db['apikeys']
movie_collection = db['movies']
movie_collection.create_index([('Title', 1)])

# would create an init mongo job for this instead of doing it this way
if apikey_collection.count_documents({}) == 0:
    api_key = os.environ.get('API_KEY')
    apikey_collection.insert_one({'apikey': api_key})

if movie_collection.count_documents({}) == 0:
    for i in range(10):
        api_url = f'https://www.omdbapi.com/?s=batman&apikey=ed8853f9&type=movie&page={i + 1}'
        response = requests.get(api_url)
        data = response.json()
        movie_collection.insert_many(data['Search'])

    print("Data inserted into MongoDB.")
else:
    print("MongoDB collection is not empty.")

movie_store = MovieStore(collection=movie_collection)
apikey_store = ApiKeyStore(collection=apikey_collection)
movie_manager = MovieManager(movie_store=movie_store, apikey_store=apikey_store)

api.add_resource(MovieHandler, '/movies',
                 resource_class_kwargs={'movie_manager': movie_manager})


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80)
