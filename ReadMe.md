# Movie Microservice

This microservice provides an API for managing a movie database.

## API Endpoints

### GET /movies

#### Retrieve a Single Movie
- Retrieves information about a single movie based on the provided title.
- Parameters:
  - `title` (required): Title of the movie.

#### Search Movies
- Retrieves a list of movies based on search parameters.
- Parameters:
  - `search` (required): Search for movie title.
  - `page_size` (optional): Number of movies to be returned per page. 10 by default.
  - `page` (optional): Pagination parameter for specifying the page to be returned. 1 by default.

### POST /movies

#### Add a Movie
- Adds a new movie to the database.
- Parameters (in JSON format):
  - `title` (required): Title of the movie.

### DELETE /movies

#### Delete a Movie
- Deletes a movie from the database.
- Parameters (in JSON format):
  - `id` (required): IMDB ID of the movie to be deleted.
  - `apikey` (required): API key for authentication.

## Usage

1. **Retrieve a Single Movie:**

   GET /movies?title=MovieTitle

2. **Retrieve list of movies based on search parameter:**

   GET /movies?search=MovieTitle&page_size=10&page=1

3. **Add movie to database:**

   POST /movies?title=MovieTitle

4. **Remove movie from database:**

   DELETE /movies?id=ImdbID&apikey=your_key
